import 'package:flutter_infinite_list/posts/posts.dart';

abstract class FetchPostRepository {
  Future<List<Post>> fetchPost(int startIndex);
}
