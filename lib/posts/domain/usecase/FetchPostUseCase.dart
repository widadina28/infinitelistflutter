import 'package:flutter_infinite_list/posts/domain/repository/FetchPostRepository.dart';
import 'package:flutter_infinite_list/posts/posts.dart';

abstract class FetchPostUseCase {
  Future<List<Post>> fetchPost(
      int startIndex, FetchPostRepository fetchPostRepository);
}
