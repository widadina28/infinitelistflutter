import 'package:flutter_infinite_list/posts/domain/repository/FetchPostRepository.dart';
import 'package:flutter_infinite_list/posts/domain/usecase/FetchPostUseCase.dart';
import 'package:flutter_infinite_list/posts/models/post.dart';

class FetchPostUseCaseImpl extends FetchPostUseCase {
  @override
  Future<List<Post>> fetchPost(
      int startIndex, FetchPostRepository fetchPostRepository) async {
    return fetchPostRepository.fetchPost(startIndex);
  }
}
