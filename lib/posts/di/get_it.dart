import 'package:flutter_infinite_list/posts/data/FetchPostRepositoryImpl.dart';
import 'package:flutter_infinite_list/posts/domain/repository/FetchPostRepository.dart';
import 'package:flutter_infinite_list/posts/domain/usecase/FetchPostUseCase.dart';
import 'package:flutter_infinite_list/posts/domain/usecase/interactor/FetchPostUseCaseImpl.dart';
import 'package:get_it/get_it.dart';

import '../posts.dart';

GetIt locator = GetIt.I;
initLocator() {
  locator.registerFactory<FetchPostUseCase>(() => FetchPostUseCaseImpl());
  locator.registerFactory<FetchPostRepository>(() => FetchRepositoryImpl());
  locator.registerFactory<PostBloc>(() => PostBloc(locator()));
}
