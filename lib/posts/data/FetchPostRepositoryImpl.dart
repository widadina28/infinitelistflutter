import 'dart:convert';
import 'package:flutter_infinite_list/posts/domain/repository/FetchPostRepository.dart';
import 'package:flutter_infinite_list/posts/models/post.dart';
import 'package:http/http.dart' as http;

const _postLimit = 20;

class FetchRepositoryImpl extends FetchPostRepository {
  final http.Client httpClient = http.Client();

  @override
  Future<List<Post>> fetchPost(int startIndex) async {
    final response = await httpClient.get(
      Uri.https(
        'jsonplaceholder.typicode.com',
        '/posts',
        <String, String>{'_start': '$startIndex', '_limit': '$_postLimit'},
      ),
    );
    if (response.statusCode == 200) {
      final body = json.decode(response.body) as List;
      return body.map((dynamic json) {
        final map = json as Map<String, dynamic>;
        return Post(
          id: map['id'] as int,
          title: map['title'] as String,
          body: map['body'] as String,
        );
      }).toList();
    }
    throw Exception('error fetching posts');
  }
}
